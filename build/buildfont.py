# Generate TTF/OTF/WOFF2 from a Fontforge SFD file and OpenType feature file
#
# Copyright 2019-2021 Rajeesh KV <rajeeshknambiar@gmail.com>
# This file is licensed under GPLv3.

import sys, os, argparse
import fontforge as ff
import pdb

def generate_font(sfd_file, feature_file, font_type, version, out_dir):

    if not os.path.exists(sfd_file):
        print("SFD file % not found", sfd_file)
        return False
    if not os.path.exists(feature_file):
        print("Feature file % not found", feature_file)
        return False
    if font_type not in ('otf', 'ttf', 'woff', 'woff2'):
        print("Only otf/ttf/woff/woff2 formats are supported")
        return False
    if not os.path.exists(out_dir):
        os.path.mkdir(out_dir)

    beta_version = False
    if int(float(version)) > 10:   #Beta version
        beta_version = True

    tmpfont = ff.open(sfd_file)
    for lookup in tmpfont.gpos_lookups + tmpfont.gsub_lookups:
        tmpfont.removeLookup(lookup)
    #For beta versions, add versio number to font name and font file name (before styles)
    if beta_version:
        #'Font-Name-Style' -> 'Font-Name$ver-Style' (though only one hyphen is allowed in PostScript FontName)
        loc_last_hyph = find_variant_pos(tmpfont.fontname, '-')
        tmpfont.fontname = tmpfont.fontname[0: loc_last_hyph] + version + tmpfont.fontname[loc_last_hyph:len(tmpfont.fontname)]
        #'Font Full Name Style' -> 'Font Full Name $ver Style'
        loc_last_space = find_variant_pos(tmpfont.fullname, ' ')
        tmpfont.fullname = tmpfont.fullname[0: loc_last_space] + ' ' + version + tmpfont.fullname[loc_last_space:len(tmpfont.fullname)]

    #Set the version
    #tmpfont.version = version      #Setting it doesn't have effect, set in SFNTName
    tmpfont.appendSFNTName('English (US)', 'Version', version)

    #tmpfont.mergeFeature(feature_file, True)    #skip non applicable features instead of erroring out
    tmpfont.mergeFeature(feature_file)

    out_font = os.path.splitext(os.path.basename(sfd_file))[0]
    loc_last_hyph = find_variant_pos(out_font, '-')
    out_font = out_font[0:loc_last_hyph] + (beta_version and version  or "") + out_font[loc_last_hyph:len(out_font)] + '.' + font_type
    out_font = os.path.join(out_dir, out_font)
    tmpfont.generate(out_font, flags=('opentype','round','dummy-dsig'))
    tmpfont.close()     #Without saving original file
    return True

def find_variant_pos(fontname, fchar):
    #TODO: handle various other styles.
    #See https://docs.fedoraproject.org/en-US/packaging-guidelines/FontsPolicy/#_style_naming
    for pl in ['Regular', 'Bold Italic', 'Bold', 'Italic']:
        loc = fontname.rfind(fchar + pl)
        if loc >= 0:
            break
    if loc == -1:
        loc = len(fontname)
    return loc


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", help="Path to SFD file")
    ap.add_argument("-f", help="Path to Feature file")
    ap.add_argument("-t", help="Font type - otf/ttf/woff2")
    ap.add_argument("-v", help="Version string")
    ap.add_argument("-o", help="Output directory")

    args = ap.parse_args()
    sfd_file = args.s if args.s else ''
    feature_file = args.f if args.f else ''
    font_type = args.t if args.t else ''
    out_dir = args.o if args.o else ''
    version = args.v if args.v else '1.0'

    generate_font(sfd_file, feature_file, font_type, version, out_dir)
